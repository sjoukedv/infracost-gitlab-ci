#!/usr/bin/env node

// This file generates a GitLab CI job to test the examples by extracting the
// examples from each README file, modifying them slightly and then writing
// them to a GitLab CI YAML file.

const fs = require('fs');
const yaml = require('js-yaml');

const examplesDir = 'examples';
const gitlabCiDir = '.gitlab-ci';
const testDataDir = 'testdata';
const exampleRegex =
  /\[\/\/\]: <> \(BEGIN EXAMPLE:\s*(.*)\)\n```.*\n((.|\n)*?)```\n\[\/\/\]: <> \(END EXAMPLE\)/gm;

// Finds all the examples in a file
// Returns an array of pairs [[exampleKey, exampleContents], ...]
function extractExamples(file) {
  const content = fs.readFileSync(file, 'utf8');
  const matches = [...content.matchAll(exampleRegex)];
  return matches.map((match) => [match[1], yaml.load(match[2])]);
}

// Extracts all the examples from a directory by reading all the README files
// Returns an array of pairs [[exampleKey, exampleContents], ...]
function extractAllExamples(examplesDir) {
  const examples = [];

  for (const dir of fs.readdirSync(examplesDir)) {
    if (!fs.statSync(`${examplesDir}/${dir}`).isDirectory()) {
      continue;
    }

    const filename = `${examplesDir}/${dir}/README.md`;

    if (fs.existsSync(filename)) {
      console.error(`Found README.md file in ${dir} was found, extracting examples`);
      try {
        examples.push(...extractExamples(filename));
      } catch(err) {
        console.error(`Error reading YAML file ${filename}: ${err}`);
      }
    }

    examples.push(...extractAllExamples(`${examplesDir}/${dir}`));
  }

  return examples;
}

// Modifies the examples by:
// 1. Passing a --dry-run flag to infracost comment
// 2. Adding a step for updating the golden file
// 3. Updating existing steps to be able to run on main branch without MR number
function fixupExamples(examples) {
  const fixupExamples = [];
  const mergeRequestTemplate = { template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml' };

  for (const [key, example] of examples) {
    let updatedExample;
    if (example.include) {
      updatedExample = Object.assign({}, example);
    } else {
      updatedExample = {include: [], ...example};
    }
    updatedExample.include.push(mergeRequestTemplate);

    for (const job of Object.values(updatedExample)) {
      if (job.rules) {
        delete job.rules;
      }

      const scriptLines = [];

      if (!job.script) {
        continue;
      }

      for (let scriptLine of job.script) {
        if (!scriptLine) {
          continue;
        }

        if (scriptLine.startsWith('git clone')) {
          scriptLine = 'git clone . /tmp/base'
          scriptLines.push(scriptLine)
        } else if (scriptLine.indexOf('infracost diff') !== -1) {
          if (scriptLine.indexOf('cd  -') !== -1) {
            scriptLine = scriptLine.replace('cd  -', '');
            scriptLines.push(`cd -`);
          }
          scriptLines.push(`find examples -type f  -name '*.tf' -o -name '*.hcl' -o -name '*.tfvars' | xargs sed -i 's/m5\.4xlarge/m5\.8xlarge/g'`);
          scriptLines.push(`find examples -type f  -name '*.tf' -o -name '*.hcl' -o -name '*.tfvars' | xargs sed -i 's/t2\.micro/t2\.medium/g'`);
          scriptLines.push(scriptLine);
        } else if (scriptLine.startsWith('infracost comment')) {
          const goldenFilePath = `${testDataDir}/${key}_comment_golden.md`;
          scriptLine = scriptLine.split('\\\n').map(s => s.trim()).join(' ');
          scriptLine = scriptLine.replace('$CI_MERGE_REQUEST_IID', '1'); // Env var not available on master
          scriptLine = `${scriptLine} --dry-run > /tmp/infracost_comment.md`;
          scriptLines.push(scriptLine);
          scriptLines.push(`[ "$UPDATE_GOLDEN_FILES" = true ] && cp /tmp/infracost_comment.md ${goldenFilePath}`);
          scriptLines.push(`[ "$UPDATE_GOLDEN_FILES" != true ] && diff ${goldenFilePath} /tmp/infracost_comment.md`);
        } else if (scriptLine.includes('$SLACK_WEBHOOK_URL')) {
          const goldenFilePath = `${testDataDir}/${key}_slack_message_golden.json`;

          scriptLines.push(`[ "$UPDATE_GOLDEN_FILES" = true ] && cp slack_message.json ${goldenFilePath}`);
          scriptLines.push(`[ "$UPDATE_GOLDEN_FILES" != true ] && diff ${goldenFilePath} slack_message.json`);
        } else {
          scriptLines.push(scriptLine);
        }
      }

      job.script = scriptLines;
    }

    fixupExamples.push([key, updatedExample]);
  }

  return fixupExamples;
}

// Write the example YAMLs
function writeTestYAMLs(examples) {
  for (const [key, example] of examples) {
    try {
      fs.writeFileSync(`${gitlabCiDir}/examples/${key}.yml`, yaml.dump(example));
    } catch (err) {
      console.error(`Error writing YAML file: ${err}`);
    }
  }
}

// Write the YAML for running all tests
function writeAllTestsYAML(examples) {
  const y = {
    stages: ['all_tests'],
  };

  for (const [key, _] of examples) {
    y[key] = {
      stage: 'all_tests',
      trigger: {
        include: `${gitlabCiDir}/examples/${key}.yml`,
        strategy: 'depend',
      },
    };
  }

  try {
    fs.writeFileSync(`${gitlabCiDir}/all-tests.yml`, yaml.dump(y));
  } catch (err) {
    console.error(`Error writing YAML file: ${err}`);
  }
}

let examples = extractAllExamples(examplesDir);
examples = fixupExamples(examples);
writeTestYAMLs(examples);
writeAllTestsYAML(examples);

console.log('DONE');
