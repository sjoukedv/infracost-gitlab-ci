# Terraform Cloud/Enterprise

This example shows how to run Infracost on GitLab CI with Terraform Cloud and Terraform Enterprise. It assumes you have set a GitLab variable for the Terraform Cloud token (`TFC_TOKEN`), which is used to run a speculative plan and fetch the plan JSON from Terraform Cloud.

To use it, add the following to your `.gitlab-ci.yml` file:

[//]: <> (BEGIN EXAMPLE: plan-json-terraform-cloud-enterprise)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/plan-json/terraform-cloud-enterprise/code

stages:
  - plan
  - infracost

cache:
  key: "${TF_ROOT}"
  paths:
    - ${TF_ROOT}/.terraform

plan:
  stage: plan
  image:
    name: hashicorp/terraform:latest
    entrypoint: [""]
  before_script:
    # IMPORTANT: add any required steps here to setup cloud credentials so Terraform can run
    # Write out the Terraform Cloud token
    - |
      cat <<EOF > /root/.terraformrc
      credentials "$TFC_HOST" {
        token = "$TFC_TOKEN"
      }
      EOF
    - cd ${TF_ROOT}
    - terraform init
  script:
    # When using TFC remote execution, terraform doesn't allow us to save the plan output.
    # So we have to save the plan logs so we can parse out the run ID and fetch the plan JSON
    - terraform plan -no-color | tee /tmp/plan_logs.txt
    - |
      echo "Parsing the run URL and ID from the logs"
      run_url=$(grep -A1 'To view this run' /tmp/plan_logs.txt | tail -n 1)
      run_id=$(basename $run_url)

      echo "Getting the run plan response from https://$TFC_HOST/api/v2/runs/$run_id/plan"
      run_plan_resp=$(wget -q -O - --header="Authorization: Bearer $TFC_TOKEN" "https://$TFC_HOST/api/v2/runs/$run_id/plan")
      echo "Extracting the plan JSON path"
      plan_json_path=$(echo $run_plan_resp | sed 's/.*\"json-output\":\"\([^\"]*\)\".*/\1/')

      echo "Downloading the plan JSON from https://$TFC_HOST$plan_json_path"
      wget -q -O plan.json --header="Authorization: Bearer $TFC_TOKEN" "https://$TFC_HOST$plan_json_path"
  artifacts:
    paths:
      - ${TF_ROOT}/plan.json
  variables:
    TFC_TOKEN: $TFC_TOKEN
    # If you're using Terraform Enterprise update `TFC_HOST` to your Terraform enterprise hostname
    TFC_HOST: app.terraform.io
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  dependencies:
    - plan
  script:
    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT}/plan.json \
                     --format=json \
                     --out-file=infracost.json

    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'

```
[//]: <> (END EXAMPLE)
