# Slack example

This example shows how to send cost estimates to Slack using GitLab CI.

Slack message blocks have a 3000 char limit so the Infracost CLI automatically truncates the middle of slack-message output formats.

For simplicity, this is based off the terraform-plan-json example, which does not require Terraform to be installed.

To use it, add the following to your `.gitlab-ci.yml` file and setup a GitLab variable for `SLACK_WEBHOOK_URL`.

[//]: <> (BEGIN EXAMPLE: slack)
```yml
variables:
  # If your terraform files are in a subdirectory, set TF_ROOT accordingly
  TF_ROOT: examples/terraform-project/code

stages:
  - infracost

infracost:
  stage: infracost
  image:
    # Always use the latest 0.10.x version to pick up bug fixes and new resources.
    # See https://www.infracost.io/docs/integrations/cicd/#docker-images for other options
    name: infracost/infracost:ci-0.10
    entrypoint: [""] # Override since we're running commands below
  script:
    # If you use private modules, add an environment variable or secret
    # called GIT_SSH_KEY with your private key, so Infracost can access
    # private repositories (similar to how Terraform/Terragrunt does).
    # - mkdir -p ~/.ssh
    # - eval `ssh-agent -s`
    # - echo "$GIT_SSH_KEY" | tr -d '\r' | ssh-add -
    # Update this to github.com, gitlab.com, bitbucket.org, ssh.dev.azure.com or your source control server's domain
    # - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
 
    # Clone the base branch of the pull request (e.g. main/master) into a temp directory.
    - git clone $CI_REPOSITORY_URL --branch=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME --single-branch /tmp/base

    # Generate an Infracost cost snapshot from the comparison branch, so that Infracost can compare the cost difference.
    - |
      infracost breakdown --path=/tmp/base/${TF_ROOT} \
                          --format=json \
                          --out-file=infracost-base.json

    # Generate an Infracost diff and save it to a JSON file.
    - |
      infracost diff --path=${TF_ROOT} \
                     --compare-to=infracost-base.json \
                     --format=json \
                     --out-file=infracost.json

    # Posts a comment to the PR using the 'update' behavior.
    # This creates a single comment and updates it. The "quietest" option.
    # The other valid behaviors are:
    #   delete-and-new - Delete previous comments and create a new one.
    #   new - Create a new cost estimate comment on every push.
    # See https://www.infracost.io/docs/features/cli_commands/#comment-on-pull-requests for other options.
    - |
      infracost comment gitlab --path=infracost.json \
                               --repo=$CI_PROJECT_PATH \
                               --merge-request=$CI_MERGE_REQUEST_IID \
                               --gitlab-server-url=$CI_SERVER_URL \
                               --gitlab-token=$GITLAB_TOKEN \
                               --behavior=update

    # Generate a slack message format from the Infracost output and post it to Slack.
    - infracost output --path=infracost.json --format=slack-message --out-file=slack_message.json
    - |
      # Skip posting to Slack if there's no cost change
      cost_change=$(cat infracost.json | jq -r "(.diffTotalMonthlyCost // 0) | tonumber")
      if [ "$cost_change" = "0" ]; then
        echo "Not posting to Slack since cost change is zero"
        exit 0
      fi

      if [ -z "$SLACK_WEBHOOK_URL" ]; then
        echo "No \$SLACK_WEBHOOK_URL variable set. Add one to your GitLab repository"
        exit 1
      fi

      curl -X POST -H "Content-type: application/json" -d @slack_message.json $SLACK_WEBHOOK_URL
  variables:
    INFRACOST_API_KEY: $INFRACOST_API_KEY
    GITLAB_TOKEN: $GITLAB_TOKEN # With `api` scope with Developer role to post merge request comments
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
```
[//]: <> (END EXAMPLE)
